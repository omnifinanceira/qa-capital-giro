require_relative 'Util.rb'

class Login < Util

    set_url '/login'

    element :elementinputusuario, :xpath, "//input[@formcontrolname='usuario']"
    element :elementinputsenha, :xpath, "//input[@formcontrolname='senha']"
    element :elementspanentrar, :xpath, "//button/span[text()=' Entrar ']"
    element :elementcontinuar, :xpath, "//button/span[text()=' Continuar ']"
     

    def preencher_usuario(usuario)
        elementinputusuario.set usuario
    end

    def preencher_senha(senha)
        elementinputsenha.set senha
    end

    def acessar_conta
        elementspanentrar.click
    end

    def acessar_conta_capital_giro(usuario = $usuarioloja, senha = $senhalojista)
        preencher_usuario(usuario)
        preencher_senha(senha)
        acessar_conta
    end

    def escolha_agente
        #binding.pry
        within(find(:xpath, "//mat-dialog-container[@role='dialog']")) do
        find(:xpath, "//mat-radio-button[@id='mat-radio-2']").click
        find(:xpath, "//button/span[text()=' Confirmar ']").click
    end
        end

    def criar_ficha
        #binding.pry
        find(:xpath, "//button[@mattooltip='Cadastrar proposta']").click
        find(:xpath, "//button[@ng-reflect-message='Financiamento PJ']").click
    end  

    def preencher_qualificacao(gerar_cnpj, gerar_nome, gerar_cpf, gerar_telefone_celular, gerar_email)
        #binding.pry
         find(:xpath, "//input[@id='p-input-cnpj']").set(gerar_cnpj)
         find(:xpath, "//input[@id='p-input-razao-social']").set(gerar_nome)
         find(:xpath, "//omni-date-picker[@id='p-input-data-constituicao']").click
         find(:xpath, "//input[@data-placeholder='Data da Constituição']").set("01071994")
         find(:xpath, "//input[@id='p-input-faturamento-anterior']").set("5000000")
         find(:xpath, "//input[@id='p-input-faturamento-atual']").set("6500000")      
         find(:xpath, "//mat-select[@id='p-select-cnae']").click
         find(:xpath, "//mat-option/span[contains(text(),' 1 -')]").click
         find(:xpath, "//input[@id='p-input-patrimonio-liquido']").set("20000000")
         find(:xpath, "//input[@id='p-input-quantidade-funcionarios']").set("20")
         find(:xpath, "//mat-select[@id='p-select-natureza-juridica']").click
         find(:xpath, "//mat-option/span[contains(text(),'102-3')]").click
         find(:xpath, "//input[@id='p-input-inscricao-estadual']").set("493980175361")
         find(:xpath, "//omni-date-picker[@id='p-input-data-registro-junta']").click
         find(:xpath, "//input[@data-placeholder='Data Registro Junta/Cartório']").set("01071994")
         find(:xpath, "//input[@id='p-input-numero-registro-junta']").set("1234567890")
         find(:xpath, "//mat-select[@id='p-select-categoria']").click
         find(:xpath, "//mat-option[@ng-reflect-value='COMERCIO']").click
         find(:xpath, "//omni-date-picker[@id='p-input-data-ultima-alteracao']").click
         find(:xpath, "//input[@data-mat-calendar='mat-datepicker-4']").set("05022000")
         find(:xpath, "//input[@id='p-input-cpf-0']").set(gerar_cpf)
         find(:xpath, "//input[@id='p-input-nome-administrador-0']").set("qa matheus")
         find(:xpath, "//input[@id='p-input-email-0']").set(gerar_email)
         find(:xpath, "//input[@id='p-input-telefone-celular-0']").set(gerar_telefone_celular)
         find(:xpath, "//mat-select[@id='p-select-forma-assinatura-0']").click
         find(:xpath, "//mat-option[@ng-reflect-value='INDIVIDUAL']").click
         find(:xpath, "//omni-date-picker[@id='p-input-data-inicio-vigencia-0']").click
         find(:xpath, "//input[@ng-reflect-placeholder='Início da Vigência']").set("17121998")
         find(:xpath, "//mat-checkbox[@id='p-select-vigencia-indeterminada-0']").click
         elementcontinuar.click
    end
    
         def preencher_endereco(gerar_cep, gerar_numero, gerar_telefone_celular, gerar_email)
            #binding.pry
            find(:xpath, "//*[@id='p-input-cep']").set(gerar_cep)
            find(:xpath, "//input[@id='p-input-numero']").set(gerar_numero)
            find(:xpath, "//input[@id='p-input-telefone']").set(gerar_telefone_celular)   
            find(:xpath, "//input[@id='p-input-email']").set(gerar_email) 
            find(:xpath, "//mat-select[@id='p-select-tipo-enderecoo']").click 
            find(:xpath, "//mat-option/span[text()=' Endereço Comercial ']").click 
            elementcontinuar.click
         end


    def cadeia_societaria(gerar_cpf, gerar_nome)
        #binding.pry
        find(:xpath, "//button[@id='p-button-adicionar-socio']").click
        find(:xpath, "//input[@id='p-modal-input-cpf-cnpj']").set(gerar_cpf)
        find(:xpath, "//input[@id='p-modal-input-nome-razao-social']").click
        find(:xpath, "//input[@id='p-modal-input-nome-razao-social']").set(gerar_nome)
        find(:xpath, "//input[@id='p-modal-input-percentual-participacao']").set("100.00")
        find(:xpath, "//button[@id='p-modal-button-salvar']").click
        elementcontinuar.click
    end

    
   def grupo_economico()
        elementcontinuar.click
    end

    def principais_clientes(gerar_nome)
        #binding.pry
        find(:xpath, "//input[@id='p-input-nome-cliente-0']").set(gerar_nome)
        find(:xpath, "//input[@id='p-input-porcentual-faturamento-0']").set("50")
        elementcontinuar.click
    end

    def principais_fornecedores(gerar_nome)
        #binding.pry
        find(:xpath, "//input[@id='p-input-nome-fornecedor-0']").set(gerar_nome)
        find(:xpath, "//input[@id='p-input-porcentual-compras-0']").set("50")
        elementcontinuar.click
    end

    def principais_produtos(gerar_nome)
        #binding.pry
        find(:xpath, "//input[@id='p-input-nome-produto-0']").set(gerar_nome)
        find(:xpath, "//input[@id='p-input-porcentual-faturamento-produto-0']").set("50")
        elementcontinuar.click
    end

    def documentos
        page.attach_file("#{Dir.pwd}\\imagens\\cpf.pdf") do
        page.find(:xpath, "//button[@ng-reflect-message='Anexar Abertura de endividamen']").click
        end
              
    end
        
    def parametros_operacionais
        #binding.pry
        elementcontinuar.click
    end

    def relatorio_visita
        #binding.pry 
        elementcontinuar.click    
    end

    def proposta_negocios
        #binding.pry
        find(:xpath, "//mat-select[@id='p-select-operacao']").click
        find(:xpath, "//mat-option/span/span[@id='Varejo-9615']").click
        find(:xpath, "//input[@id='p-input-valor-emprestimo']").set("1500000")
        find(:xpath, "//input[@id='p-input-parcelas']").set("36")
        find(:xpath,  "//input[@id='p-input-valor-tarifa-cadastro']").set("100000")
        find(:xpath,  "//input[@id='p-input-valor-seguro']").set("200000") 
        find(:xpath,  "//input[@id='p-input-data-liberacao']").set("10122021")
        find(:xpath,  "//input[@id='p-input-vencimento-primeira-parcela']").set("02012022")  
        find(:xpath,  "//button[@id='p-button-calculo']").click 
         elementcontinuar.click
    end

   def garantias
       #binding.pry
        find(:xpath, "//button[@id='p-button-adicionar-garantia']").click
        within( find(:xpath, "//mat-dialog-container[@id='mat-dialog-2']") )
        find(:xpath, "//mat-select[@formcontrolname='tipoGarantia']").click
        find(:xpath, "//mat-option/span/span[text()='Veículo']").click
        find(:xpath, "//input[@formcontrolname='valorGarantia']").set("3000000")
        find(:xpath, "//mat-select[@id='p-modal-select-categoria']").click
        find(:xpath, "//mat-option/span[text()=' AUTOMOVEL ']").click
        find(:xpath, "//mat-select[@id='p-modal-select-marca']").click
        find(:xpath, "//mat-option/span[text()=' VOLVO ']").click
        find(:xpath, "//mat-select[@id='p-modal-select-ano-modelo']").click
        find(:xpath, "//mat-option/span[text()=' 2015 ']").click
        find(:xpath, "//mat-select[@id='p-modal-select-ano-fabricacao']").click
        find(:xpath, "//mat-option/span[text()=' 2015 ']").click
        find(:xpath, "//mat-select[@id='p-modal-select-modelo']").click
        find(:xpath, "//mat-option/span[text()=' V60 ']").click
        find(:xpath, "//mat-select[@id='p-modal-select-versao']").click
        find(:xpath, "//mat-option/span[text()=' T-5 KINETIC 2.0 245CV 5P G ']").click
        find(:xpath, "//input[@id='p-modal-input-placa']").set("IVG-0407")
        find(:xpath, "//mat-select[@id='p-modal-select-uf']").click
        find(:xpath, "//mat-option/span[text()=' SP ']").click
        find(:xpath, "//input[@id='p-modal-input-renavam']").set("79804241222")
        find(:xpath, "//mat-dialog-actions/div/button/span[contains(text(),'Salvar')]").click
        find(:xpath, "//div/button/span[text()=' Salvar ']").click
    end
end




