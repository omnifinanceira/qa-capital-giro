require 'cpf_cnpj'
require 'faker'
require_relative 'util.rb'

class GeradorRandomico < Util
    
    attr_accessor :cpf, :cnpj, :email, :nome, :cep, :numero

    def initialize
        gerar_cpf
        gerar_cnpj
        gerar_email
        gerar_nome
        gerar_cep
        gerar_numero
        gerar_rg
        gerar_data_rg
    end

    def gerar_cpf
        cpf = CPF.generate(true)
        return cpf
    end

    def gerar_cnpj
        cnpj = CNPJ.generate(true)
        return cnpj
    end

    def gerar_email
        email = Faker::Internet.email
        return email
    end

    def gerar_nome
        nome = Faker::Name.name
        return 'Teste ' + nome
    end

    def gerar_cep
        cep = '01435-001'
        return cep
    end

    def gerar_numero(inicial = 01,final = 99999)
        numero = rand(inicial..final)
        return numero.to_s
    end

    def gerar_rg
        rg = rand(1111..99999).to_s +  rand(1111..99999).to_s
        return rg
    end

    def gerar_data_rg
        data_hoje = Time.now
        data_correta = Time.new(((data_hoje.year) - 7),data_hoje.month,data_hoje.day).strftime("%d/%m/%Y")
        return data_correta
    end

    def gerar_data_nascimento
        data_hoje = Time.now
        # data_nascimento = Time.new(((data_hoje.year) - 18),data_hoje.month,data_hoje.day).strftime("%d/%m/%Y")
        data_nascimento = Time.new(((data_hoje.year) - 20),data_hoje.month,data_hoje.day).strftime("%d/%m/%Y")
        return data_nascimento  
    end

    def gerar_telefone_celular
        numero = '11' + '9' + gerar_numero(1111,9999).to_s + '-' + gerar_numero(1111,9999).to_s
        return numero
    end

end