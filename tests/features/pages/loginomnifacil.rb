require_relative 'Util.rb'

class LoginOmniFacil < Util

    set_url 'https://hml-omnifacil2.omni.com.br/hml/pck_login.prc_login'

    element :elementinputusuarioomnifacil, :xpath, "//input[@formcontrolname='usuario']"
    element :elementinputsenhaomnifacil, :xpath, "//input[@formcontrolname='senha']"
    element :elementconectar, :xpath, "//button/span[text()=' Entrar ']"

    def preencher_usuario_omnifacil(usuario)
        elementinputusuarioomnifacil.set usuario
    end

    def preencher_senha_omnifacil(senha)
        elementinputsenhaomnifacil.set senha
    end

    def clicar_em_conectar
        elementconectar.click
    end

    def acessar_conta_omnifacil_mesa(usuario = $usuariomesa, senha = $senhamesa)
        preencher_usuario_omnifacil(usuario)
        preencher_senha_omnifacil(senha)
        clicar_em_conectar   
    end
end