class OmniFacil < SitePrism::Page

    element :elementmesadecredito, :xpath, "//a[@id='sel-sistema[11]']"
    element :elementhabilitarlistaagente, :xpath, "//span[@id='select2-p-agente-container']"
    element :elementselecionaragente, :xpath, "//li[text()='1631 - CDCL SNV SUL - CURITIBA-PR']"
    element :elementvalidar, :xpath, "//button[@id='bt-validar']"

    def acessar_mesa_credito
        elementmesadecredito.click
    end

    def acessar_novo_menu
        find(:xpath, "//a[@id='sel-sistema[1]']").click
    end

    def listar_agente
        elementhabilitarlistaagente.click
        elementselecionaragente.click
        elementvalidar.click
    end

    def pesquisar_numero_proposta(numero_proposta)
        find(:xpath, "//input[@name='paramProposta-inputEl']").set(numero_proposta)
        find(:xpath, "//span[@id='buttonPesquisar-btnIconEl']").click
        find(:xpath, "//div[@id='gridview-1029-hd-CDCL SUL']").click

        @nova_janela = window_opened_by {
            find(:xpath, "//a[text()='#{numero_proposta}']").click
            }
    end

    def fechar_pop_up
        begin
            within_frame(find(:xpath, "//iframe")[:id]) do
                within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                    if find(:xpath, "//div[@id='popup_content']").visible?
                        find(:xpath, "//div/input[@id='popup_ok']").click
                    end
                end
            end
        rescue Exception => ex
         ex.message
        end
    end

    def aprovacao_proposta
        within_window @nova_janela do
            fechar_pop_up
            within_frame(find(:xpath, "//iframe")[:id]) do
                within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                    find(:xpath, "//input[@id='buttonCollapse']").click
                    find(:xpath, "//input[@class='botaoVerde']").click
                    accept_confirm('Deseja finalizar a proposta com status APROVADA?')
                end
            end
        end
    end


    def preencher_check_list
        within_window @nova_janela do
            within_frame(find(:xpath, "//iframe")[:id]) do
                within_frame(all(:xpath, "//frameset")[0].all(:xpath, "//frame")[1][:name]) do
                    find(:xpath, "//input[@value='#008_COMPROVANTE DE RENDA']").click
                    find(:xpath, "//textarea[@name='p_parecer']").set('Teste QA CDCL')
                    find(:xpath, "//input[@value='Validar']").click
                    accept_confirm('Deseja finalizar a proposta?')

                    find(:xpath, "//font[@class='txtAzulBold12']").text == "Dados enviados com sucesso!\nProposta:31664478 - APROVADA."
                end
            end
        end
    end

    def selecionar_menu(menu = nil)
        if !menu.nil?
            find(:xpath, "//li/a[text()=' #{menu}']").click
        end
    end

    def selecionar_submenu(submenu = nil)
        if !submenu.nil?
            find(:xpath, "//ul/li/a[text()='#{submenu}']").click 
        end
    end

    def pesquisar_contrato
        within_frame(find(:xpath, "//iframe")[:id]) do
            within_frame(find(:xpath, "//frameset").all(:xpath, "//frame")[0][:name]) do
                find(:xpath, "//input[@name='p_contrato']").set('101631063920821')
                find(:xpath, "//input[@value='Pesquisar']").click
            end
        end
    end

    def formalizar_contrato
        find(:xpath,"//input[@value='Formalizar']" )
    end
end