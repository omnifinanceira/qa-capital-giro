require_relative 'geradorrandomico.rb'

class OmniMais < GeradorRandomico

    set_url ''

    element :elementcontinuar, :xpath, "//i[@class='fa fa-arrow-right mlBt']"
    element :elementanalisar, :xpath, "//a[text()=' Analisar ']"
    element :elementsalvar, :xpath, "//button[text()='Salvar']"

    def initialize
        @sql = Queries.new
    end

    def selecionar_opcao(opcao)
        $wait.until{find(:xpath, "//app-button-selector/div/div[text()='#{opcao}']").click}
    end

    def selecionar_operacao(operacao = "//option[2]" )
        sleep(15)
        find(:xpath, "//select[@name='codigosOperacoes']").find(:xpath, operacao).select_option
    end

    # def selecionar_operacao_premium
    #     find(:xpath, "//select[@name='codigosOperacoes']").find(:xpath, "//option[text()='8683 - CDC PREMIUM 2 S/J SNV SUL']").select_option
    # end

    def clicar_em_continuar
        $wait.until{elementcontinuar.click}   
    end

    def clicar_em_analisar
        elementanalisar.click
    end

    def clicar_em_salvar
        elementsalvar.click
    end

    def selecionar_vendedor_loja
        sleep(10)
        find(:xpath, "//span[@class='chevron fa fa-chevron-down']").click
        find(:xpath, "//app-auto-complete[@formcontrolname='vendedorId']/ul/li/a[text()='Não cadastrado']").click
    end

    def pegar_horario
        horarioagora = Time.new
        return horarioagora.strftime('%H:%M')
    end

    def fechar_alerta_ficha_existente_loja
        begin
            if find(:xpath, "//h3[text()='Ficha em andamento encontrada!']").visible?
                find(:xpath, "//button[text()=' Iniciar nova ficha ']").click
            end
        rescue Exception => ex
         ex.message
        end
    end

    def preencher_dados_cliente_loja(cpf, nascimento, tabela = "//div[2]/div/select/option[2]")
       
        find(:xpath, "//input[@formcontrolname='cpf']").set(cpf)
        find(:xpath, "//input[@formcontrolname='dataNascimento']").set(nascimento)
        fechar_alerta_ficha_existente_loja
        find(:xpath, "//select[@name='classeProfissional']").find(:xpath, "//option[text()='AUTONOMO']").click
        find(:xpath, "//input[@formcontrolname='renda']").set(gerar_numero(4000,8000).to_s + '00')
        find(:xpath, "//input[@formcontrolname='valorSolicitado']").set(gerar_numero(400,5000).to_s + '00')
        find(:xpath, "//select[@formcontrolname='tabela']").find(:xpath, tabela).select_option
        find(:xpath,"//input[@formcontrolname='cep']").set('06184140')
            find(:xpath, "//input[@name='numero']").set('66')
    end

    def selecionar_parcelas_loja
        @quantidade_parcela = 'P12'
        $wait.until{find(:xpath, "//label[@for='#{@quantidade_parcela}']").select_option}
        end

    def selecionar_seguro(seguro)
        find(:xpath,"//a[@class='icon-full']").click
        find(:xpath,"//p[text()='#{seguro}']").click
        find(:xpath,"//a[text()=' Salvar ']").click  
    end

    def armazenar_dados_proposta
        parcelas_valor_auxiliar = find(:xpath, "//label[@for='#{@quantidade_parcela}']/span").text.split("\n")

        $quantidade_parcela_omni_mais = parcelas_valor_auxiliar[0].gsub('x','')
        $valor_parcela_omni_mais = parcelas_valor_auxiliar[1].gsub('R$ ','')
        # puts "Quantidade: " + $quantidade_parcela_omni_mais + ' ' + "Valor: " + $valor_parcela_omni_mais
    end

    def preencher_detalhes_cliente_loja(telefone)
        @ddd = '55'+telefone.split('')[0..1].join('')
        @telefone = telefone.split('')[2..10].join('')

        
        aguardar_elemento_ficar_invisivel("//div[@id='loader']")
        #pegar nome no banco
        limpar_e_escrever("//input[@formcontrolname='nomeCliente']",'Suely Dos Santos Amorim')
        limpar_e_escrever("//input[@formcontrolname='emailCliente']", gerar_email)
        limpar_e_escrever("//input[@formcontrolname='telefoneCelularCliente']", telefone)
        find(:xpath, "//select[@formcontrolname='sexoCliente']").find(:xpath, "//option[text()='FEMININO']").select_option
        find(:xpath, "//select[@formcontrolname='estadoCivilCliente']").find(:xpath, "//option[text()='SOLTEIRO']").select_option
        find(:xpath, "//select[@formcontrolname='profissaoCliente']").all(:xpath, "//option[text()='AUTONOMO']")[0].select_option
        limpar_e_escrever("//input[@formcontrolname='empresaCliente']",'OMNI' )
        limpar_e_escrever("//input[@formcontrolname='telefoneEmpresaCliente']",'1133653500')
        limpar_e_escrever("//input[@formcontrolname='telefone']",'11998765432')
        #all(:xpath, "//input[@formcontrolname='telefone']")[0].native.clear
        #all(:xpath, "//input[@formcontrolname='telefone']")[0].set('11998765432')
        limpar_e_escrever("//input[@formcontrolname='nomeReferencia']",'JULIANA AMIGA')
        limpar_e_escrever("//input[@formcontrolname='telefoneReferencia']",'11997543627')
    end

    def validar_sms
        find(:xpath, "//a[text()=' Enviar Token para o celular ']").click
        sleep(2)
        @token = ''
        aguardar_elemento_ficar_visivel("//input[@formcontrolname='codigoConfirmacao']")
        nova_janela = open_new_window
        within_window nova_janela do
            @sms = Sms.new
            @sms.load
            @sms.pesquisar_sms(@ddd,@telefone)
            @token = @sms.pegar_token
        end
        find(:xpath, "//input[@formcontrolname='codigoConfirmacao']").set(@token)
        aguardar_elemento_ficar_visivel("//div[text()=' Token Validado com Sucesso ']")
        end

    def selecionar_endereco_correspondencia
        find(:xpath, "//select[@formcontrolname='enderecoCorrespondencia']").find(:xpath, "//option[text()='RESIDENCIAL']").select_option
    end

    def valor_mercadoria
        pegarvalor = all(:xpath, "//div[@class='form-group show-total']/div/p")[1].text
        @valormercadoria =  pegarvalor.gsub(/[^0-9]/,'') 
        return @valormercadoria
    end

    def preencher_demais_dados_cliente_loja
        page.attach_file("#{Dir.pwd}\\imagens\\foto_pessoa.jpg") do
            page.find(:xpath, "//a[text()=' Não foi possível fotografar o cliente?']").click
        end
        find(:xpath, "//div/div[2]/div/div[1]/p/i").click
        valor_mercadoria
        find(:xpath, "//select[@formcontrolname='tiposMercadoria']").find(:xpath, "//option[text()=' ELETRODOMÉSTICOS E ELETRÔNICOS ']").select_option
        find(:xpath, "//input[@formcontrolname='mercadoria']").set('CELULAR')
        find(:xpath, "//input[@formcontrolname='valorProduto']").set(@valormercadoria)
    end

    def aguardar_elemento_ficar_invisivel(xpath)
        @horaatual = pegar_horario
        contagem = (Time.parse(@horaatual) + 120).strftime('%H:%M')
        begin
            while find(:xpath, "#{xpath}").visible? && contagem != @horaatual
                @horaatual = pegar_horario
            end
            result = (contagem != @horaatual)
        rescue
        end
        return result
    end

    def aguardar_elemento_ficar_visivel(xpath)
        @horaatual = pegar_horario
        contagem = (Time.parse(@horaatual) + 120).strftime('%H:%M')
        begin
            while !find(:xpath, "#{xpath}").visible? && contagem != @horaatual
                @horaatual = pegar_horario
            end
            result = (contagem != @horaatual)
            rescue
        end
        return result
    end

    def acessar_aba_fichas_aprovadas
        find(:xpath,"//a[@href='#aprovadas']").click 
    end

    def pesquisar_proposta()
        find(:xpath,"//a[@title='Filtrar fichas de análise']").click 
        find(:xpath,"//input[@name='numero']").set($proposta)
        sleep(15)
        find(:xpath,"//a[@title='Ver ficha detalhada']").click
    end

    def fechar_negocio
        find(:xpath,"//div[2]/div[1]/button").click
    end

    def preencher_dados_complementares
        find(:xpath,"//select[@id='user-id']").find(:xpath,"//option[text()='Registro Geral - RG']").select_option
        find(:xpath,"//input[@name='numeroDocumentoCliente']").set('472551844')
        find(:xpath,"//input[@name='orgaoDocumentoCliente']").set('ssp')
        find(:xpath,"//input[@name='emissaoDocumentoCliente']").set('01052015')
        find(:xpath,"//input[@name='nomeMaeClienteText']").set('Maria da Silva')
        find(:xpath,"//input[@name='ufn']").check
        find(:xpath,"//select[@name='nacionalidadeCliente']").find(:xpath,"//option[text()='BRASILEIRA']").select_option
        find(:xpath,"//select[@name='naturalUfCliente']").find(:xpath,"//option[text()='SP - São Paulo']").select_option
        find(:xpath,"//select[@name='naturalCidadeCliente']").find(:xpath,"//option[text()='SAO PAULO']").select_option
        find(:xpath,"//input[@id='user-patrimony']").set('10000' + '00')
        clicar_em_salvar
    end

    def anexar_documentos
        sleep(10)
        find(:xpath,"//button[text()='Anexar Documentos']").click
        page.attach_file("#{Dir.pwd}\\imagens\\rg.png") do
            page.find(:xpath, "//div[text()=' RG ']").click
        end
        page.attach_file("#{Dir.pwd}\\imagens\\cpf.pdf") do
            page.find(:xpath, "//div[text()=' CPF ']").click
        end
        find(:xpath, "//button[text()='Confirmar seleção']").click
        find(:xpath, "//button[text()=' Enviar Documentos para Formalização ']").click
        
    end

    def assinatura_digital
        find(:xpath, "//button[text()='Assinar Documentos']").click
        find(:xpath, "//label[text()='Enviar Token']").click
        sleep(20)
        nova_janela = open_new_window
        within_window nova_janela do
            @sms = Sms.new
            @sms.load
            sleep(15)
            @sms.pesquisar_sms(@ddd,@telefone)
            @token = @sms.pegar_token
            @tokenccb = @token.slice(0,@token.size-6)
            #@tokenseguro = @token.slice(6,@token.size)
        end
        find(:xpath, "//a[text()='Abrir Contratos']").click
        find(:xpath, "//button[@id='ds_hldrBdy_btnDSInPersonIDControl_btnInline']").click
        find(:xpath, "//button[@id='action-bar-btn-continue']").click
        binding.pry
        all(:xpath, "//input[contains(@id, 'tab-form-element')]")[1].set(@tokenccb)
        all(:xpath, "//input[contains(@id, 'tab-form-element')]")[2].set(@tokenccb)
        # all(:xpath, "//input[contains(@id, 'tab-form-element')]")[5].set(@tokenseguro)
        # all(:xpath, "//input[contains(@id, 'tab-form-element')]")[6].set(@tokenseguro)
        find(:xpath, "//button[@id='action-bar-btn-finish']").click
        find(:xpath, "//button[@data-qa='dialog-submit']").click
        aguardar_elemento_ficar_invisivel
        find(:xpath, "//button[text()='Enviar para formalização']").click
    end
    
end