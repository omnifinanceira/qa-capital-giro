require_relative 'Util.rb'

class Sms < Util

    set_url 'https://hmg-api.omni.com.br/sms/messages'

    element :elementinputddd, :xpath, "//input[@id='ddd']"
    element :elementinputtelefone, :xpath, "//input[@id='fone']"
    element :elementbuttonpesquisar, :xpath, "//button[text()='Pesquisa']"
   
    def pesquisar_sms(ddd,telefone)
        elementinputddd.set ddd
        elementinputtelefone.set telefone
        elementbuttonpesquisar.click
    end
     

    def pegar_token
        textosms = all(:xpath,"//tr")[1].all(:xpath, "//td")[3].text
        token = textosms.gsub(/[^0-9]/,'')
        return token
    end

end