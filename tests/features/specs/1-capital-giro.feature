#language:pt
@1 @capital-giro
Funcionalidade: Criar proposta de financiamento PJ

Cenario: Criar proposta de financiamento PJ

    Quando logo como capital de giro
    E seleciono o agente
    E clico no botão  para adicionar a ficha
    E preencho a qualificação da empresa
    E preencho o endereço da empresa
    E preencho a cadeia societária
    E avanço o grupo economico 
    E preencho os principais clientes
    E preencho os principais fornecedores
    E preencho os principais produtos
    E preencho os documentos
    E preencho parametros operacionais
    E preencho relatorio da visita
    E preencho a proposta de negocios
    E preencho a garatia

