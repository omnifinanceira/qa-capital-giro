#language:pt
@2 @cdc-premium

Funcionalidade: Criar proposta de CDC Premium

Cenario: Criar proposta de CDC Premium

    Quando logo como cdc premium
    E clico em adicionar ficha
    E clico no tipo de operacao premium
    E seleciono o codigo de operacao
    E seleciono o vendedor
    E preencho as informacoes do cliente <cpf> e <nascimento> premium
    # E seleciono o seguro premium
    E seleciono as parcelas em resultado parcial
    E preencho os detalhes do cliente e <telefone>
    E valido o token via sms
    E preencho os enderecos do cliente
    E preencho os demais dados do cliente
    E valido os dados enviados da proposta
    E acesso o omnifacil com o usuario da mesa 
    E acesso o menu mesa de credito
    E pesquiso e acesso a proposta
    E realizo o aceite da proposta
    E valido o checklist e se a proposta foi aprovada
    E acesso omnimais como usuario premium
    E pesquiso o numero da proposta na aba fichas aprovadas
    E acesso a timeline da proposta para fechar negocio
    E preencho os dados complementares
    E anexo os documentos e envio para a formalizacao
    E realizo a assinatura dos documentos via sms e envio para formalizacao
    E acesso o omnifacil com usuario da formalizacao
    E acesso o novo menu 
    E seleciono o agente correspondente
    E acesso a fila de formalizacao
    E busco pelo numero do contrato gerado
    Entao faco a formalizacao

    Exemplos:
    |cpf            |nascimento    |telefone      |
    |"00849556511"  |"27/03/1979"  |"11989091459" |
