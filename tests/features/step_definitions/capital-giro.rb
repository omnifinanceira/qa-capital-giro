Quando('logo como capital de giro') do
  @login = Login.new
  @home = Home.new
  @gerador = GeradorRandomico.new
  @login.load
  @login.acessar_conta_capital_giro
 
end
  
Quando('seleciono o agente') do
  @login.escolha_agente
end

Quando('clico no botão  para adicionar a ficha') do
   @login.criar_ficha
end

Quando('preencho a qualificação da empresa') do
  @login.preencher_qualificacao(@gerador.gerar_cnpj, @gerador.gerar_nome, @gerador.gerar_cpf, 
  @gerador.gerar_telefone_celular, @gerador.gerar_email)
 end

Quando('preencho o endereço da empresa') do
  @login.preencher_endereco(@gerador.gerar_cep, @gerador.gerar_numero, @gerador.gerar_telefone_celular,
  @gerador.gerar_email)
end

Quando('preencho a cadeia societária') do
  @login.cadeia_societaria(@gerador.gerar_cpf, @gerador.gerar_nome)
end

Quando('avanço o grupo economico') do
  @login.grupo_economico
end

Quando('preencho os principais clientes') do
  @login.principais_clientes(@gerador.gerar_nome)
end

Quando('preencho os principais fornecedores') do
  @login.principais_fornecedores(@gerador.gerar_nome)
end

Quando('preencho os principais produtos') do
  @login.principais_produtos(@gerador.gerar_nome)
end

Quando('preencho os documentos') do
  @login.documentos
end

Quando('preencho parametros operacionais') do
  @login.parametros_operacionais
end

Quando('preencho relatorio da visita') do
  @login.relatorio_visita
end

Quando('preencho a proposta de negocios') do
  @login.proposta_negocios
end

Quando('preencho a garatia') do
  @login.garantias
end


