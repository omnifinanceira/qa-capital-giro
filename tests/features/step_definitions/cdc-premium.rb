Quando('logo como cdc premium') do
    @loginomnifacil = LoginOmniFacil.new
    @omnifacil = OmniFacil.new
    @login = Login.new
    @home = Home.new
    @omnimais = OmniMais.new
    @login.load
    @login.acessar_conta_omnimais_premium
  end

  Quando('clico no tipo de operacao premium') do
    @omnimais.selecionar_opcao('CDC Premium')
  end

  Quando('preencho as informacoes do cliente {string} e {string} premium') do |cpf, nascimento|
    @omnimais.preencher_dados_cliente_loja(cpf,nascimento)
    @omnimais.clicar_em_continuar  
    sleep(5)
  end

  Quando('seleciono o seguro premium') do
    @omnimais.selecionar_seguro('SEGUROS MÓVEIS PREMIUM')
  end

  Quando('acesso omnimais como usuario premium') do
    @login.load
    @login.acessar_conta_omnimais_premium
  end